# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class TakeEvent(Event2):
    NAME = "take"

    def perform(self):
        if not self.object.has_prop("takable"):
            self.add_prop("object-not-takable")
            return self.take_failed()
        if self.object in self.actor:
            self.add_prop("object-already-in-inventory")
            return self.take_failed()
        self.object.move_to(self.actor)
        self.inform("take")

    def take_failed(self):
        self.fail()
        self.inform("take.failed")


class TakeWithEvent(Event3):
    NAME = "take-with"

    def perform(self):
        if not self.object2.has_prop("takablewith"):
            self.add_prop("object-not-takablewith")
            return self.take_failed()
        if not self.object2.has_prop("takewith"):
            self.fail()
            return self.inform("object-not-takablewith")    
        if self.object2 in self.actor:
            self.add_prop("object-already-in-inventory")
            return self.take_failed()
        self.object2.move_to(self.actor, self.objet, self.object2)
        self.inform("take-with")

    def take_with_failed(self):
        self.fail()
        self.inform("take-with.failed")