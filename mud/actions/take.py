# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import TakeEvent

class TakeAction(Action2):
    EVENT = TakeEvent
    RESOLVE_OBJECT = "resolve_for_take"
    ACTION = "take"

class TakeWithAction(Action3):
	EVENT = TakeEvent
	RESOLVE_OBJECT = "resolve_for_use"
	RESOLVE_OBJECT2 = "resolve_for_take"
	ACTION = "take-with"