# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .effect import Effect2, Effect3, Effect4
from mud.events import LightOnEvent, LightOffEvent

class LightOnEffect(Effect2):
    EVENT = LightOnEvent

class LightOffEffect(Effect3):
    EVENT = LightOffEvent

class LightWithEffect(Effect3):
    EVENT = LightOnEvent

class LightOffWithEffect(Effect4):
    EVENT = LightOffEvent