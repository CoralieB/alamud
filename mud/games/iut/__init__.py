# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from mud.actions import (
    GoAction, TakeAction, TakeWithAction, LookAction, InspectAction, OpenAction,
    OpenWithAction, CloseAction, TypeAction, InventoryAction,
    LightOnAction, LightOffAction, LightWithAction, LightOffWithAction, DropAction, DropInAction,
    PushAction, TeleportAction, EnterAction, LeaveAction,
)

import mud.game

def make_rules():
    GAME = mud.game.GAME
    DIRS = list(GAME.static["directions"]["noun_at_the"].values())
    DIRS.extend(GAME.static["directions"]["noun_the"].values())
    DIRS.extend(GAME.static["directions"]["normalized"].keys())
    DETS = "(?:l |le |la |les |une |un |)"

    return (
        (GoAction       , r"(?:aller |)(%s)" % "|".join(DIRS)),
        (TakeAction     , r"(?:p|prendre) %s(\S+)" % DETS),
        (TakeWithAction , r"(?:p|prendre) %s(\S+) (?:avec) %s(\w+)" % (DETS,DETS)),
        (LookAction     , r"(?:r|regarder)"),
        (InspectAction  , r"(?:r|regarder|lire|inspecter|observer) %s(\S+)" % DETS),
        (OpenAction     , r"(?:ouvrir|répondre) %s(\S+)" % DETS),
        (OpenWithAction , r"(?:ouvrir|répondre) %s(\S+) avec %s(\w+)" % (DETS,DETS)),
        (OpenAction     , r"(?:ouvrir) %s(\S+)" % DETS),
        (OpenWithAction , r"(?:ouvrir) %s(\S+) avec %s(\w+)" % (DETS,DETS)),
        (CloseAction    , r"fermer %s(\S+)" % DETS),
        (TypeAction     , r"(?:taper|[eé]crire|r[eé]pondre) (\S+)$"),
        (InventoryAction, r"(?:inventaire|inv|i)$"),
        (LightOnAction  , r"allumer %s(\S+)" % DETS),
        (LightOffAction , r"[eé]teindre %s(\S+)" % DETS),
        (LightWithAction , r"allumer %s(\S+) avec %s(\w+)" % (DETS,DETS)),
        (LightOffWithAction , r"[eé]teindre %s(\S+) avec %s(\w+)" % (DETS,DETS)),
        (DropAction     , r"(?:poser|laisser) %s(\S+)" % DETS),
        (DropInAction   , r"(?:poser|laisser) %s(\S+) (?:dans |sur |)%s(\S+)" % (DETS,DETS)),
        (PushAction     , r"(?:appuyer|pousser|presser|donner)(?: sur|) %s(\S+)" % DETS),
        (TeleportAction , r"tele(?:porter|) (\S+)"),
        (EnterAction    , r"entrer"),
        (LeaveAction    , r"sortir|partir"),
        #(LightWithAction, r"(?:allumer|embraser|enflammer) %s(\S+) (?: avec) %s(\S+) " % (DETS,DETS)),
    )
